db.iventory.insertMany([
	{
		"name": "Captain America Shield",
		"price": 50000,
		"qty": 17,
		"company": "Hydra and Co."
	},
	{
		"name": "Mjolnir",
		"price": 75000,
		"qty": 24,
		"company": "Asgard Production"
	},
	{
		"name": "Iron Man Suit",
		"price": 25400,
		"qty": 25,
		"company": "Stark Industries"
	},
	{
		"name": "Eye of Agamoto".
		"price": 28000,
		"qty": 51,
		"company": "Sanctum company"
	},
	{
		"name": "Iron Spider Suit",
		"price": 30000,
		"qty": 24,
		"company": "Stark Industries"
	},


])

//Queery Operators = allows us to be more flexible when querying in mongoDB, we can opt to find, update and delete documents based on some conditions instead of just specific criterias.


/*
	Comparison Query operators
	 - $gt and $gte
	 = syntaax:
	  db.collections.find({field: {$gt: value}})
	  db.collections.find({field: {$gte: value}})

	 $gt = greater than, to find values that are greater than the given values.
	 $gte = greter than or equal,


	 $lt = less than
	 $lte = less than or equal

	 $ne = not equal, returns a documents that values are not equal to the given value

	 $in = allows us to find douments that satisfy either of the specified criteria

	 REMINDER: although you can express this query using $or operator, choose the $in operator rather than $or operator when performing equality checks on the same 



*/

	db.collections.find({field: {$gt: value}})

	db.iventory.find({price: {$gte: 75000}})


	db.iventory.find({qty: {$lte: 20}})

	db.iventory.find({qty : {$ne : 10}})


	db.iventory.find({price : {$in : [25400, 30000] }})



//mini activity

	db.iventory.find({price: {$gte: 50000}})

	db.iventory.find({qty: {$in: [24,16]}})

/*
	UPDATE and DELETE 
		2 arguments namely : query criteria, update
*/

 	db.iventory.updateMany(
 		{qty: {$lte: 24}}, 
 		{$set: {isActive: true}}

 		)


 // $unset = to delete a field

  db.iventory.updateMany(
  		
  		{price: {$lt: 28000}},
  		{$set: {qty: 17}}

  	)

  /*
		LOGICAL OPERATORS
		 $and = need to satisfies ALL the given conditions
		 syntax:
		  db.collections.find({$and: [{criteria1},{criteria2}]})
  */

  	db.iventory.find(

  			{$and: 
  			[{price: {$gte: 50000}},
  			{qty:17}]}

  		)

  /*
	or $or = returns documents that satisfies either of the given conditions
	snytax: db.collections.find({$or: [{criteria1},{criteria2}]})
  */

  	db.iventory.find({$or:
  		[
  			{qty: {$lt: 24}},
  			{isActive: false}
  		]
  	});

  	db.iventory.find({$or:
  		[
  			{qty: {$lte: 24}},
  			{price: {$lte: 30000}},
  			
  		]
  	}
  	{$set: {isActive: true}});


  	db.iventory.updateMany(
  		{$or: 
  			[
  				{qty: {$lte:24}},
  				{price: {$lte: 30000}}
  			]
  		}, 
  		{
  			$set: 
  				{isActive: true}
  		}

  	)


  	/*
		Evaluation Quesry Operator
		$regex
		syntax: 
		{field: {$regex: /pattern/}} = case sensitive inquery
		{field: {$regex: /pattern/ , $options: /pattern/}}
  	*/

  	db.iventory.find({name: {$regex: 'S'}})

  	db.iventory.find(
  		{
  			$and:
  				[
  					{
  						name: {$regex: 'i', $options: '$i'}
  					},
  					{
  						price: {$gt: 70000}
  					}
  				]
  		})


  	/*
		Field Projection
		 - allows to us to hide or show properties of a returned documents after a query. when dealing with a comples data structures, there might be instances when a fields
  	*/

  	db.collections.find({criteria},{field: 1}) //inclusion
  	db.collections.find({criteria},{field: 0}) // exclusion

  	db.iventory.find({},{name: 1})


  	db.iventory.find({},{qty: 0})

  	db.iventory.find({},{name: 0, price: 0, _id: 0})

  	//REMINDER: 


 	db.iventory.find(
  		{
  			$and:
  				[
  					{
  						name: {$regex: 'A'}
  					},
  					{
  						price: {$gt: 30000}
  					},

  				]},
  				{
  					name:1, price:1, _id:0
  				}


  		)

db.iventory.find({$and:
  				[
  					{
  						name: {$regex: 'A'}
  					},
  					{
  						price: {$gt: 30000}
  					},

  				]},{name: 1, price: 1, _id: 0})